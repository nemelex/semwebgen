# README

## Requirements

You must have `python3` and `virtualenv` installed.

Actually if you are ok and able to install all the needed pip packages
in your system, you can do just that without `virtualenv` - just skip
the creation and activation of virtualenv and do everything else the same.

Be aware that if you don't want to use virtualenv,
on many GNU/Linux distros `pip` is the python 2 package manager,
and pip for python 3 is called some other name, for example in ubuntu 14.04
it's `pip3`, and so you will have to change `pip` to `pip3` when performing
installation of required packages.

## Install

```
mkdir semweb_virtualenv
cd semweb_virtualenv
virtualenv -p python3 .
source bin/activate
# now your shell prompt should have (semweb_virtualenv) in the beginning
git clone https://nemelex@bitbucket.org/nemelex/semwebgen.git
cd semwebgen
pip install -r requirements.txt 
```

## Examples

There are a few examples bundled with the program. They are located in
examples directory. You can generate rdf and html files from them using the
following command
(assuming your pwd is the same you're left with after installation).

```
export PYTHONPATH="$(pwd)/src"
./src/semwebgen/generate_personal_rdf.py --in examples/pupkin.json --outrdf pupkin.xml
./src/semwebgen/generate_personal_rdf.py --in examples/pupkin.json --outfriendsrdf friends.xml
./src/semwebgen/generate_personal_rdf.py --in examples/pupkin.json --outhtml pupkin.html
./src/semwebgen/generate_personal_rdf.py --in examples/pupkin.json --outhtml pupkin.html --no-friends-info
```

## Filling your own info

I suggest you take `pupkin.json` example, edit it, replace its info with your
own and do the same command as in [Examples](#examples)

After this you can serve the generated rdf and html files as static files
somewhere on the web, and other people can add link to your rdf file in friends
section of the json file and generate their pages with you in the list of their
friends.

## How to use web interface

```
./src/webfrontend/semweb_website.py
```

It will start a website on port *5000*.

**WARNING**: this web interface will run in debug mode if launched like this. Either remove the
`app.debug = True` line in [semweb_website.py](src/webfrontend/semweb_website.py), or use uwsgi or some
flask-compatible web server.