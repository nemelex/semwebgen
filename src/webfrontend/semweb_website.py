#!/usr/bin/env python3
# encoding: utf-8
"""
@author:     Filipp Blagoveschenskiy

@copyright:  Copyright 2015 Filipp Blagoveschenskiy

@license:

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

@contact:    b_filip@zoho.com
"""
import json

from functools import partial
from flask import Flask, request, session, render_template, abort, send_from_directory
from flask.helpers import url_for
from os import urandom, path, pardir, makedirs
import sh

from semwebgen.generate_personal_rdf import write_person_files_from_json_file

app = Flask(__name__)
app.secret_key = urandom(24)

PASSWORD = 'iceicecreamcream'

HTTP_FORBIDDEN_CODE = 403

PERSON_FOLDER = path.join(app.root_path, 'person')
JSON_FILENAME = 'person.json'
RDF_FILENAME = 'person.xml'
HTML_FILENAME = 'person.html'
FRIENDS_RDF_FILENAME = 'friends.xml'
JSON_FULL_PATH = path.join(PERSON_FOLDER, JSON_FILENAME)
RDF_FULL_PATH = path.join(PERSON_FOLDER, RDF_FILENAME)
HTML_FULL_PATH = path.join(PERSON_FOLDER, HTML_FILENAME)
FRIENDS_RDF_FULL_PATH = path.join(PERSON_FOLDER, FRIENDS_RDF_FILENAME)

PACKAGE_PATH = path.dirname(path.abspath(__file__))

_generation_script_path = path.join(PACKAGE_PATH, pardir, 'semwebgen', 'generate_personal_rdf.py')
GENERATION_SCRIPT = sh.Command(_generation_script_path)


def open_json_file_for_writing():
    if not path.exists(PERSON_FOLDER):
        makedirs(PERSON_FOLDER)
    return open(JSON_FULL_PATH, 'w', encoding='utf-8')


class Authorization():
    _key = 'authorized'
    _value = 'true'

    @staticmethod
    def check():
        return session.get(Authorization._key, None) == Authorization._value

    @staticmethod
    def set():
        session[Authorization._key] = Authorization._value


def add_scheme_to_url(url):
    return url if "://" in url else "http://" + url


def valid_password():
    return request.form.get('password', None) == PASSWORD


@app.route('/')
def show_authorization_page():
    return app.send_static_file('authorization.html')


@app.route('/generate/login', methods=['POST'])
def show_login_authorization_result():
    if valid_password():
        Authorization.set()
        return app.send_static_file('successful_login.html')
    else:
        return app.send_static_file('fail_login.html')


def show_unless_unauthorized(response_generator):
    if not Authorization.check():
        return "You are not logged in"
    return response_generator()


@app.route('/generate/form')
def show_form_page():
    def show():
        with open(JSON_FULL_PATH, 'r') as json_file:
            personal_json = json.load(json_file)
        converted_personal_json = {
            key.replace(' ', '_'): value for (key, value) in personal_json.items()
        }
        converted_personal_json['interests'] = '\n'.join(
            [interest for interest in personal_json.get('interests', [])]
        )
        converted_personal_json['friends'] = '\n'.join([
            friend['rdf'] + ' | ' + friend['html']
            for friend in personal_json.get('friends', [])
        ])
        return render_template('fields.html', **converted_personal_json)

    return show_unless_unauthorized(show)


def represents_int(string):
    if string is None:
        return False
    try:
        int(string)
        return True
    except ValueError:
        return False


def add_birthday_from_form(json_):
    if all((
            field in request.form and represents_int(field)
            for field in ('birthday_year', 'birthday_month', 'birthday_day')
    )):
        json_['birthday'] = {
            'year': int(request.form['birthday_year']),
            'month': int(request.form['birthday_month']),
            'day': int(request.form['birthday_day'])
        }


def add_friends_from_form(json_):
    friend_delimiter = '|'
    if 'friends' in request.form:
        friends = []
        for line in request.form.get('friends', '').split('\n'):
            if friend_delimiter in line:
                split_line = line.strip().split(friend_delimiter, 1)
                friends.append({
                    'rdf': add_scheme_to_url(split_line[0].strip()),
                    'html': add_scheme_to_url(split_line[1].strip())
                })
        if friends:
            json_['friends'] = friends


def add_interests_from_form(json_):
    if 'interests' in request.form:
        interests = [
            stripped_line for stripped_line in [
                line.strip() for line
                in request.form['interests'].split('\n')
            ]
            if stripped_line
        ]
        if interests:
            json_['interests'] = interests


def add_nonempty_string_from_form(json_, json_key, form_field):
    if form_field in request.form and request.form[form_field]:
        json_[json_key] = request.form[form_field]


def json_from_post_request():
    the_json = {}
    for json_key, form_field in (
            ('first name', 'first_name'), ('last name', 'last_name'),
            ('nickname', 'nickname'), ('avatar', 'avatar_url')
    ):
        add_nonempty_string_from_form(the_json, json_key, form_field)
    add_birthday_from_form(the_json)
    add_friends_from_form(the_json)
    add_interests_from_form(the_json)
    return the_json


def check_checkbox(name, value) -> bool:
    """
    :param name: name of the checkbox to check
    :param value: expected value of the checkbox, set in html
    :return: True if such checkbox was indeed checked, False otherwise
    """
    return name in request.form and request.form[name] == value


@app.route('/generate/result', methods=['POST'])
def show_result_page():
    def show():
        use_friends_info = check_checkbox('use_friends_info', 'true')
        new_json = json_from_post_request()
        with open_json_file_for_writing() as json_outfile:
            json.dump(new_json, json_outfile)

        if use_friends_info:
            generate_file = partial(write_person_files_from_json_file, html_out=HTML_FULL_PATH)
        else:
            generate_file = partial(
                write_person_files_from_json_file, html_no_friends_out=HTML_FULL_PATH
            )
        generate_file(
            json_in=JSON_FULL_PATH,
            rdf_out=RDF_FULL_PATH,
            friends_rdf_out=FRIENDS_RDF_FULL_PATH
        )
        return render_template(
            'result.html',
            rdf_url=url_for('send_person_file', filename=RDF_FILENAME),
            html_url=url_for('send_person_file', filename=HTML_FILENAME),
            friends_rdf_url=url_for('send_person_file', filename=FRIENDS_RDF_FILENAME)
        )

    return show_unless_unauthorized(show)


@app.route('/person/<filename>')
def send_person_file(filename):
    allowed_filenames = [JSON_FILENAME, RDF_FILENAME, HTML_FILENAME, FRIENDS_RDF_FILENAME]
    if filename not in allowed_filenames:
        abort(HTTP_FORBIDDEN_CODE)
    return send_from_directory(PERSON_FOLDER, filename, cache_timeout=0)


def person_json_exists():
    return path.isfile(JSON_FULL_PATH)


def create_person_json():
    with open_json_file_for_writing() as json_file:
        json.dump({}, json_file)


@app.before_first_request
def create_person_json_if_missing():
    if not person_json_exists():
        create_person_json()


if __name__ == '__main__':
    app.debug = True
    app.run()