#!/usr/bin/env python3
# encoding: utf-8

"""
@author:     Filipp Blagoveschenskiy

@copyright:  Copyright 2015 Filipp Blagoveschenskiy

@license:

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

@contact:    b_filip@zoho.com
"""

import jinja2


def _guess_autoescape(template_name):
    if template_name is None or '.' not in template_name:
        return False
    ext = template_name.rsplit('.', 1)[1]
    return ext in ('html', 'htm', 'xml')


def _create_jinja2_html_environment(directory_path):
    return jinja2.Environment(
        autoescape=_guess_autoescape,
        loader=jinja2.FileSystemLoader(directory_path),
        extensions=['jinja2.ext.autoescape']
    )


def create_html_template(directory_path, filename):
    return _create_jinja2_html_environment(directory_path) \
        .get_template(filename)