#!/usr/bin/env python3
# encoding: utf-8
"""
@author:     Filipp Blagoveschenskiy

@copyright:  Copyright 2015 Filipp Blagoveschenskiy

@license:

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

@contact:    b_filip@zoho.com
"""
import sys
import argparse
import json
from datetime import date
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

from functools import partial
import os
import functools
from rdflib import Graph, BNode, URIRef, Literal
from rdflib.namespace import FOAF, Namespace, RDF
from dateutil.relativedelta import relativedelta

from commoncode.jinja2stuff import create_html_template


__all__ = []
__version__ = 0.1
__date__ = '2015-02-18'

DEBUG = 1

LOCAL_ONTOLOGY = Namespace('http://local.ontology#')
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATES_DIR = os.path.join(THIS_DIR, 'templates')

REQUEST_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 '
                  '(KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'
}


class CLIError(Exception):
    """Generic exception to raise and log different fatal errors."""

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


def get_by_keys(collection, *args):
    try:
        current_level = collection
        for key in args:
            current_level = current_level[key]
        return current_level
    except KeyError:
        return None


class Person:
    def __init__(self, person_json):
        get = functools.partial(get_by_keys, person_json)

        self.first_name = get('first name')
        self.last_name = get('last name')
        self.nickname = get('nickname')
        birthday_year = get('birthday', 'year')
        birthday_month = get('birthday', 'month')
        birthday_day = get('birthday', 'day')
        if (birthday_year is not None and
                birthday_month is not None and
                birthday_day is not None):
            self.birthday = date(birthday_year, birthday_month, birthday_day)
        else:
            self.birthday = None
        self.avatar_url = get('avatar')
        self.interests = get('interests') or None

        # populate self.friends with pairs of the following type
        # ("url_to_rdf", "url_to_html")
        self.friends = [
            (dict_of_urls['rdf'], dict_of_urls['html'])
            for dict_of_urls in (get('friends') or [])
        ]

        self.node = BNode()

    def __get_first_name_triplet(self):
        return self.node, FOAF.givenName, Literal(self.first_name)

    def __get_last_name_triplet(self):
        return self.node, FOAF.familyName, Literal(self.last_name)

    def __get_nickname_triplet(self):
        return self.node, FOAF.nick, Literal(self.nickname)

    def __get_foaf_birthday_triplet(self):
        return (
            self.node, FOAF.birthday,
            Literal("{0}-{1}".format(self.birthday.month, self.birthday.day))
        )

    def __get_foaf_age_triplet(self):
        return (
            self.node, FOAF.age,
            Literal(relativedelta(date.today(), self.birthday).years)
        )

    def __get_avatar_url_triplet(self):
        return self.node, FOAF.depiction, URIRef(self.avatar_url)

    def __get_interests_triplets(self):
        for interest in self.interests:
            yield (self.node, FOAF.topic_interest, Literal(interest))

    def __get_friends_urls_triplets(self):
        for friend_urls_pair in self.friends:
            # in each pair first part is url to RDF, second is url to HTML
            friend = BNode()
            friend_rdf = URIRef(friend_urls_pair[0])
            friend_html = URIRef(friend_urls_pair[1])
            yield (self.node, FOAF.knows, friend)
            yield (friend_rdf, RDF.type, FOAF.PersonalProfileDocument)
            yield (friend_rdf, FOAF.maker, friend)
            yield (friend, FOAF.homepage, friend_html)
            yield (friend, RDF.type, FOAF.Person)

    def _get_personal_profile_document_triplets(self):
        this_document = URIRef("")
        return [
            (this_document, RDF.type, FOAF.PersonalProfileDocument),
            (this_document, FOAF.maker, self.node),
            (this_document, FOAF.primaryTopic, self.node)
        ]

    def get_triplets(self):
        triplets = []
        triplets.extend(self._get_personal_profile_document_triplets())
        triplets.append((self.node, RDF.type, FOAF.Person))
        if self.first_name is not None:
            triplets.append(self.__get_first_name_triplet())
        if self.last_name is not None:
            triplets.append(self.__get_last_name_triplet())
        if self.nickname is not None:
            triplets.append(self.__get_nickname_triplet())
        if self.birthday is not None:
            triplets.append(self.__get_foaf_birthday_triplet())
            triplets.append(self.__get_foaf_age_triplet())
        if self.avatar_url is not None:
            triplets.append(self.__get_avatar_url_triplet())
        if self.interests is not None:
            for interest in self.__get_interests_triplets():
                triplets.append(interest)
        if self.friends is not None:
            for triplet in self.__get_friends_urls_triplets():
                triplets.append(triplet)
        return triplets

    @staticmethod
    def _create_friend_dict(friend):
        friend_dict = {
            'html_url': friend[1],
            'full_name': None,
            'avatar_url': None
        }
        rdf_url = friend[0]
        graph = Graph()
        graph.parse(rdf_url, format='xml')
        makers_of_that_rdf = list(graph.objects(
            subject=URIRef(rdf_url), predicate=FOAF.maker
        ))
        # if this is not true, that rdf is not a valid rdf
        if len(makers_of_that_rdf) == 1:
            that_person = makers_of_that_rdf[0]
            full_name = ""

            def get_single_object(predicate, default):
                try:
                    return next(graph.objects(subject=that_person, predicate=predicate))
                except StopIteration:
                    return default

            full_name += get_single_object(FOAF.givenName, "")
            family_name = get_single_object(FOAF.familyName, None)
            if family_name is not None:
                full_name += " " + family_name
            friend_dict['full_name'] = full_name

            friend_dict['avatar_url'] = get_single_object(FOAF.depiction, None)
            return friend_dict

    def create_html(self, use_friends_info):
        jinja2_template_substitutions = {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'nickname': self.nickname,
            'birthday': (
                str(self.birthday) if self.birthday is not None
                else None
            ),
            'avatar_url': self.avatar_url,
            'interests': self.interests,
            'friends': [
            ]
        }

        if use_friends_info:
            template = create_html_template(TEMPLATES_DIR, 'person_with_friends_info.html')

            for friend in self.friends:
                jinja2_template_substitutions['friends'].append(Person._create_friend_dict(friend))

        else:
            template = create_html_template(TEMPLATES_DIR, 'person_without_friends_info.html')
            jinja2_template_substitutions['friends'] = [
                {'html_url': friend[1]} for friend in self.friends
            ]

        html_string = template.render(jinja2_template_substitutions)
        return html_string

    def get_friends_graph(self):
        graph = Graph()
        for pair_of_urls in self.friends:
            # in each pair [0] is rdf url, [1] is html url
            graph.parse(pair_of_urls[0], format='xml')
        return graph


def write_html_to_file(html_string, file):
    file.write(html_string)


def create_graph(person):
    """Creates and returns RDF graph created from infile"""
    graph = Graph()
    for triplet in person.get_triplets():
        graph.add(triplet)
    return graph


def write_rdf(person, rdf_output_file):
    graph = create_graph(person)
    rdf_output_file.write(graph.serialize(format='pretty-xml'))


def write_html(person, html_output_file, use_friends_info):
    person_html = person.create_html(use_friends_info)
    write_html_to_file(person_html, html_output_file)


def write_friends_rdf(person, friends_rdf_output_file):
    friends_graph = person.get_friends_graph()
    friends_rdf_output_file.write(friends_graph.serialize(format='pretty-xml'))


class NoOutputFileSpecifiedException(Exception):
    pass


def can_be_opened(x):
    return isinstance(x, str) or isinstance(x, int)


def write_person_files_from_json_file(
        json_in,
        rdf_out=None, html_out=None, friends_rdf_out=None,
        html_no_friends_out=None
):
    if all([
            elem is None
            for elem in (rdf_out, html_out, friends_rdf_out, html_no_friends_out)
    ]):
        raise NoOutputFileSpecifiedException()

    open_parameters_list = {
        'json_in': {'mode': 'r', 'encoding': 'utf-8'},
        'rdf_out': {'mode': 'wb'},
        'html_out': {'mode': 'w', 'encoding': 'utf-8'},
        'friends_rdf_out': {'mode': 'wb'},
        'html_no_friends_out': {'mode': 'w', 'encoding': 'utf-8'}
    }

    file_objects = {}
    files_to_close = []
    try:
        # open files which were passed as filenames or file descriptors
        # instead of file objects
        args = locals()
        for name, open_arguments in open_parameters_list.items():
            if can_be_opened(args[name]):
                file = open(args[name], **open_parameters_list[name])
                files_to_close.append(file)
            elif args[name] is None:
                continue
            else:
                file = args[name]
            file_objects[name] = file

        parsed_json = json.load(file_objects['json_in'])
        person = Person(parsed_json)

        if 'rdf_out' in file_objects:
            write_rdf(person, file_objects['rdf_out'])
        if 'html_out' in file_objects:
            write_html(person, file_objects['html_out'], use_friends_info=True)
        if 'friends_rdf_out' in file_objects:
            write_friends_rdf(person, file_objects['friends_rdf_out'])
        if 'html_no_friends_out' in file_objects:
            write_html(person, file_objects['html_no_friends_out'], False)
    finally:
        for file in files_to_close:
            file.close()


def main():
    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_version_message = '%%(prog)s %s' % (
        program_version)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Filipp Blagoveschenskiy on %s.
  Copyright 2015 Filipp Blagoveschenskiy

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(
            description=program_license,
            formatter_class=RawDescriptionHelpFormatter
        )
        parser.add_argument(
            '-V', '--version', action='version',
            version=program_version_message
        )
        parser.add_argument(
            '--in', metavar='INFILE',
            type=argparse.FileType('r'),
            default=sys.stdin,
            help='input file'
        )

        output_rdf_option_name = '--outrdf'
        output_html_option_name = '--outhtml'
        output_friends_rdf_option_name = '--outfriendsrdf'

        parser.add_argument(
            output_html_option_name, metavar='OUTFILE',
            help='save personal page as rdf file'
        )
        parser.add_argument(
            output_rdf_option_name, metavar='OUTFILE',
            help='save personal page as html file'
        )
        parser.add_argument(
            output_friends_rdf_option_name, metavar='OUTFILE',
            help='save rdf information of all friends'
        )

        parser.add_argument(
            '--no-friends-info', action='store_true',
            help='do not download additional information about friends'
        )

        # Process arguments
        args = parser.parse_args()

        if args.no_friends_info and args.outfriendsrdf is not None:
            print(
                "ERROR: Can't use {0} and {1} together."
                .format(output_friends_rdf_option_name, '--no-friends-info')
            )
            return 2

        # Can't use args.in because in is a keyword
        input_file = args.__getattribute__('in')

        try:
            if args.no_friends_info:
                write_person_files = partial(
                    write_person_files_from_json_file, html_out=args.outhtml
                )
            else:
                write_person_files = partial(write_person_files_from_json_file, html_out=args.outhtml)
            write_person_files(
                input_file, rdf_out=args.outrdf, friends_rdf_out=args.outfriendsrdf
            )
        except NoOutputFileSpecifiedException:
            print(
                'ERROR: At least one of {0}, {1} or {2} '
                'arguments must be specified'
                .format(
                    output_html_option_name,
                    output_rdf_option_name,
                    output_friends_rdf_option_name
                )
            )
            return 2

        return 0
    except KeyboardInterrupt:
        # handle keyboard interrupt
        return 0
    except Exception as e:
        if DEBUG:
            raise e
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help" + '\n')
        return 2


if __name__ == "__main__":
    sys.exit(main())